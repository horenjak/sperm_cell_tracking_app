import cv2
import numpy as np
import torch
from torchvision import models, transforms
from PIL import Image
import random


FRAME_MEMORY = 30
NUMBER_OF_CLASSES = 4
CLASSES = ['bundle', 'more sperm cells', 'other', 'sperm cell', 'skip']
DISTANCE_TOLERANCE = 40

class KF():
    def __init__(self, x):
        dt = 1
        self.A = np.array([[1, 0, dt, 0],
                            [0, 1, 0, dt],
                            [0, 0, 1,  0],
                            [0, 0, 0,  1]])
        self.H = np.array([[1, 0, 0, 0],
                            [0, 1, 0, 0]])
        self.R_VCL = 1 * np.eye(2)
        self.R = 50 * np.eye(2)

        self.Q = np.array([[0.1, 0, 0, 0],
                            [0, 0.1, 0, 0],
                            [0, 0, 0.01, 0],
                            [0, 0, 0, 0.01]])
        self.P_center = np.eye(self.Q.shape[0]) * 1000
        self.P_head = np.eye(self.Q.shape[0]) * 1000
        self.x_center = x
        self.x_head = x
        self.log_x_head = []
        self.log_x_center = []
    
    def predict(self):
        x_pre_head = self.A.dot(self.x_head)
        x_pre_center = self.A.dot(self.x_center)
        P_pre_head = self.A.dot(self.P_head).dot(self.A.T) + self.Q
        P_pre_center = self.A.dot(self.P_center).dot(self.A.T) + self.Q

        self.x_head = x_pre_head
        self.x_center = x_pre_center
        self.P_head = P_pre_head
        self.P_center = P_pre_center

        return self.x_center
    
    def correct(self, yt_center, yt_head):
        y_pre_head = self.H.dot(self.x_head)
        y_pre_center = self.H.dot(self.x_center)
        vt_head = yt_head - y_pre_head
        vt_center = yt_center - y_pre_center

        St_center = self.H.dot(self.P_center).dot(self.H.T) + self.R
        St_head = self.H.dot(self.P_head).dot(self.H.T) + self.R_VCL
        Wt_center = self.P_center.dot(self.H.T).dot(np.linalg.inv(St_center))
        Wt_head = self.P_head.dot(self.H.T).dot(np.linalg.inv(St_head))

        x_pre_center = self.x_center + Wt_center.dot(vt_center)
        x_pre_head = self.x_head + Wt_head.dot(vt_head)

        P_pre_center = (np.eye(4) - Wt_center.dot(self.H)).dot(self.P_center)
        P_pre_head = (np.eye(4) - Wt_head.dot(self.H)).dot(self.P_head)

        self.x_head = x_pre_head
        self.x_center = x_pre_center
        self.P_head = P_pre_head
        self.P_center = P_pre_center
        return self.x_center   

    def log(self):
        x, y, _, _ = self.x_head
        predicted_point = np.array((int(x), int(y)), dtype=np.int32)
        self.log_x_head.append(predicted_point)
        x, y, _, _ = self.x_center
        predicted_point = np.array((int(x), int(y)), dtype=np.int32)
        self.log_x_center.append(predicted_point)

def video_resize(video_path, resize_factor = 2, id=1):
    video = cv2.VideoCapture(str(video_path))
    fps = video.get(cv2.CAP_PROP_FPS)
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH) * resize_factor)
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT) * resize_factor)

    video_out = cv2.VideoWriter(f"video_resize_{id}.avi", cv2.VideoWriter_fourcc(*'XVID'), fps, (width, height))

    while True:
        ret, frame = video.read()
        if not ret:
            break
        resized_frame = cv2.resize(frame, (width, height))
        video_out.write(resized_frame)
    
    video.release()
    video_out.release()
    
    return cv2.VideoCapture(f"video_resize_{id}.avi")


def overlapping(rect1, rect2, tolerance=0.5, inner=False):
    rect1_x1, rect1_y1 = rect1[0], rect1[1]
    rect1_x2, rect1_y2 = rect1[0] + rect1[2], rect1[1] + rect1[3]
    rect2_x1, rect2_y1 = rect2[0], rect2[1]
    rect2_x2, rect2_y2 = rect2[0] + rect2[2], rect2[1] + rect2[3]

    area1 = rect1[2] * rect1[3]
    area2 = rect2[2] * rect2[3]

    x_dist = max(0, min(rect1_x2, rect2_x2) - max(rect1_x1, rect2_x1))
    y_dist = max(0, min(rect1_y2, rect2_y2) - max(rect1_y1, rect2_y1))
    area_intersection = x_dist * y_dist

    if x_dist > 0 and y_dist > 0:
        if inner:
            if min(area1, area2) * tolerance < area_intersection:
                return True
        else:
            if max(area1, area2) * tolerance < area_intersection:
                return True

    return False

def calculate_average_path(path):
    num_points = len(path)
    average_path = []
    window_size = 30

    for i in range(num_points):
        if i < window_size or i >= num_points - window_size:
            average_path.append(path[i])
        else:
            sum_x = sum([point[0] for point in path[i-window_size:i+window_size+1]])
            sum_y = sum([point[1] for point in path[i-window_size:i+window_size+1]])
            average_point = [sum_x / (2*window_size + 1), sum_y / (2*window_size + 1)]
            average_path.append(average_point)

    return average_path

class Detect_Sperm:

    def __init__(self, video_output, video_number, height, width, video_scale, output_dir, resize_factor, frame_rate):
        self.video_output = video_output
        self.frame_number = 0
        self.sperm_count= 0
        self.sperm_cells = {}
        self.video_number = video_number
        self.height = height
        self.width = width
        self.frame_rate = frame_rate
        self.video_scale = video_scale
        self.output_dir = output_dir
        self.resize_factor = resize_factor

        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.classification_model = models.resnet18(weights='IMAGENET1K_V1')
        self.classification_model.fc = torch.nn.Linear(self.classification_model.fc.in_features, NUMBER_OF_CLASSES)
        self.classification_model.load_state_dict(torch.load('trained_resnet_17_02_25.pth', map_location=device, weights_only=True))
        self.classification_model.to(device)
        self.classification_model.eval()
        
        self.transform = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
            ])
        
    def background_isolation(self, video):
        frames = []
        frames_without_halo = []

        lower_white = np.array([235, 235, 235], dtype=np.uint8)
        upper_white = np.array([255, 255, 255], dtype=np.uint8)
    
        while True:
            ret, frame = video.read()
            if not ret:
                logger.debug(f"video end")
                break
            mask_white = cv2.inRange(frame, lower_white, upper_white)
            frame_without_halo = frame.copy()
            frame_without_halo[mask_white > 0] = [60,60,60]
            frames.append(frame)
            frames_without_halo.append(frame_without_halo)
        
        frames_array = np.array(random.sample(frames_without_halo, 20))
        logger.debug(f"array created array len: {len(frames_without_halo)}")
        median_background = np.median(frames_array, axis=0).astype(np.uint8)
        logger.info(f"background calculated")
        return median_background, frames, frames_without_halo
    
    def draw_bboxs(self, frame):
        frame_copy = frame.copy()
        for sperm_id, sperm_cell in self.sperm_cells.items():
            if sperm_cell["last_frame"] != self.frame_number:
                continue
            x, y, w, h = sperm_cell['bbox']
            _, _, predicted_class =  sperm_cell['path'][-1]
            cv2.rectangle(frame_copy, (x, y), (x + w, y + h), (0, 255, 0), 2)

            text_x = x
            text_y = y - 10
            if text_y < 0:
                text_y = y + h + 10

            cv2.putText(frame_copy, str(sperm_id), (text_x, text_y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            if predicted_class != 4:
                cv2.putText(frame_copy, str(CLASSES[predicted_class]), (text_x + 20, text_y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)
            else:
                _, _, predicted_class =  sperm_cell['path'][-2]
                cv2.putText(frame_copy, str(CLASSES[predicted_class]), (text_x + 20, text_y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 2)


        self.video_output.write(np.array(frame_copy))
        return

    def detect_sperm_cells (self, contours, frame, image_foreground_preprocessed):
        frame_copy = frame.copy()

        if len(contours) == 0:
            return frame, {}
        sperms = {}
        sperm_id = 0
        largest_sperm_area = cv2.contourArea(max(contours, key=cv2.contourArea))
        for contour in contours:
            if cv2.contourArea(contour) < largest_sperm_area * 0.2:
                continue
            x, y, w, h = cv2.boundingRect(contour)

            M = cv2.moments(contour)
            if M['m00'] != 0:
                cx = M["m10"] / M["m00"]
                cy = M["m01"] / M["m00"]
            sperm_id += 1

            new_x = int(max(0, x - 0.15 * w))
            new_y = int(max(0, y - 0.15 * h))
            new_w = int(min(self.width, x + w * 1.3))
            new_h = int(min(self.height, y + h * 1.3))
            
            sperm_cell_image = image_foreground_preprocessed[new_y:new_h, new_x:new_w]
            sperm_cell_image = cv2.dilate(sperm_cell_image,  np.ones((3, 3), np.uint8),iterations=4)

            sperm_head_contour = None
            i = 0
            while True:
                sperm_cell_image = cv2.erode(sperm_cell_image, np.ones((3, 3), np.uint8) , iterations=1)
                old_sperm_head_contour = sperm_head_contour
                sperm_head_contour, _ = cv2.findContours(sperm_cell_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
                i += 1
                if len(sperm_head_contour) == 0 or i > 100:
                    sperm_head_contour = old_sperm_head_contour
                    break
                contour_areas = [cv2.contourArea(contour) for contour in sperm_head_contour]
                largest_contour_index = contour_areas.index(max(contour_areas))
                if contour_areas[largest_contour_index] <= 30:
                    if contour_areas[largest_contour_index] <= 20:
                        sperm_head_contour = old_sperm_head_contour
                    break
            
            adjusted_sperm_head_contour = []
            for head_contour in sperm_head_contour:
                adjusted_contour = head_contour.copy()
                for point in adjusted_contour:
                    point[0][0] += new_x
                    point[0][1] += new_y
                adjusted_sperm_head_contour.append(adjusted_contour)

            head_pint = (0,0)
            blank = np.zeros(frame.shape[0:2])
            for head_contour in adjusted_sperm_head_contour:
                image1 = cv2.drawContours(blank.copy(), [head_contour], -1, (255, 255, 255), thickness=cv2.FILLED)
                image2 = cv2.drawContours(blank.copy(), [contour], -1, (255, 255, 255), thickness=cv2.FILLED)
                intersection = np.logical_and(image1, image2)
                if intersection.any():
                        M = cv2.moments(head_contour)
                        if M['m00'] != 0:
                            head_pint = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            if head_pint == (0,0):
                M = cv2.moments(cv2.contourArea(max(adjusted_sperm_head_contour, key=cv2.contourArea)))
                if M['m00'] != 0:
                    head_pint = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            cv2.circle(frame_copy, head_pint, 2, (100, 0, 200), -1)
            h_x, h_y = head_pint

            cv2.circle(frame_copy, (int(cx), int(cy)), 2, (0, 0, 255), -1)
            if h_x == 0 or h_y == 0:
                sperms[sperm_id] = {'bbox': (x, y, w, h) , 'center': (int(cx), int(cy)), 'head': (int(cx), int(cy)) }
            else:
                sperms[sperm_id] = {'bbox': (x, y, w, h) , 'center': (int(cx), int(cy)), 'head': (int(h_x), int(h_y))}

        return frame, sperms

    def bbox (self, video):
        video_background, frames, frames_without_halo  = self.background_isolation(video)
        logger.info(f"number of frames for processing is: {len(frames)}")
        cache.set('total_frames', len(frames))

        for self.frame_number, (frame, frame_without_halo)  in enumerate(zip(frames, frames_without_halo)):
            logger.info(f"processing frame {self.frame_number} out of {len(frames)}")
            cache.set('progress', self.frame_number)

            image_foreground =  cv2.absdiff(frame_without_halo, cv2.convertScaleAbs(video_background))
            image_foreground_grayscale = cv2.cvtColor(image_foreground, cv2.COLOR_BGR2GRAY)
            blur = cv2.GaussianBlur(image_foreground_grayscale,(5,5),0)
            _,image_foreground_preprocessed = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            image_foreground_preprocessed = cv2.dilate(image_foreground_preprocessed,  np.ones((3, 3), np.uint8),iterations=1)

            contours, _ = cv2.findContours(image_foreground_preprocessed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            frame, detected_bboxs = self.detect_sperm_cells(contours, frame, image_foreground_preprocessed)
            self.find_match(detected_bboxs, frame)
            self.draw_bboxs(frame)
        last_frame = frames[-1].copy()
        self.plot_all_paths(last_frame)
        self.save_results()
        return detected_bboxs
    
    def predict_class(self, image):
        with torch.no_grad():
            output = self.classification_model(image)

        probabilities = torch.nn.functional.softmax(output[0], dim=0)
        predicted_class = torch.argmax(probabilities).item()
        return predicted_class

    def find_match(self, detected_sperm, frame):
        '''matches sperm cells across frames to keep track of them'''
        unmatched = list(detected_sperm.keys())
        for sperm_cell_number, entry in self.sperm_cells.items(): 
            kalman = entry['kalman']
            last_pint = entry['last_frame']
            for bbox_id, sperm_cell in detected_sperm.items():
                if overlapping(sperm_cell['bbox'], entry['bbox']) and  bbox_id in unmatched and last_pint > self.frame_number - FRAME_MEMORY:
                    entry['bbox'] = sperm_cell['bbox']
                    entry['center'] = sperm_cell['center']
                    entry['head'] = sperm_cell['head']
                    entry['detected'] += 1
                    entry['last_frame'] = self.frame_number
                    kalman.correct(np.array(entry['center'], dtype=np.float32), np.array(entry['head'], dtype=np.float32))
                    unmatched.remove(bbox_id)
                    break

            x, y, vel1, vel2 = kalman.predict()
            kalman.log()

            velocity = np.sqrt(np.array(vel1)**2 + np.array(vel2)**2)
            predicted_point = np.array((int(x), int(y)), dtype=np.int32)
            x, y, w, h = entry['bbox']

            ###YOU CAN TRY CHANGING THIS----------------------------------------###

            # new_x = int(max(0, x - 0.15 * w))
            # new_y = int(max(0, y - 0.15 * h))
            # new_w = int(min(self.width, x + w * 1.3))
            # new_h = int(min(self.height, y + h * 1.3))

            new_x = int(max(0, x - 0.01 * w))
            new_y = int(max(0, y - 0.01 * h))
            new_w = int(min(self.width, x + w * 1.02))
            new_h = int(min(self.height, y + h * 1.02))

            ###-----------------------------------------------------------------###

            sperm_cell_image = frame[new_y:new_h, new_x:new_w]

            if self.frame_number % 2 == 0:
                predicted_class = self.predict_class(self.transform(Image.fromarray(sperm_cell_image)).unsqueeze(0))
            else:
                predicted_class = 4
            entry['path'].append((predicted_point, self.frame_number, predicted_class))
            entry['velocities'].append((velocity))

        for bbox_id in unmatched:
            match = False
            for sperm_id, entry in self.sperm_cells.items(): 
                sperm_cell = detected_sperm[bbox_id]
                last_pint, _, _ = entry['path'][-1]
                at_frame = entry['last_frame']
                distance = np.linalg.norm(last_pint - sperm_cell['center'])
                if distance < DISTANCE_TOLERANCE and at_frame > self.frame_number - FRAME_MEMORY:
                    entry['bbox'] = sperm_cell['bbox']
                    entry['center'] = sperm_cell['center']
                    entry['head'] = sperm_cell['head']
                    entry['last_frame'] = self.frame_number
                    kalman = entry['kalman']
                    kalman.correct(np.array(entry['center'], dtype=np.float32), np.array(entry['head'], dtype=np.float32))
                    kalman.log()
                    match = True
            if not(match):
                self.add_new_sperm_cell(detected_sperm[bbox_id], frame)
        return
    
    def add_new_sperm_cell (self, sperm_cell, frame):
        x, y = sperm_cell['center']
        kalman = KF(np.array([x, y, 0, 0], dtype=np.float32))
        self.sperm_cells[self.sperm_count] = {'kalman': kalman , 'path': [], 'velocities': [], 'bbox': sperm_cell['bbox'], 'center': sperm_cell['center'],  'head': sperm_cell['head'], 'last_frame': self.frame_number, 'detected': 0}

        x, y, w, h = sperm_cell['bbox']
        sperm_cell_image = frame[y:y+h, x:x+w]
        predicted_class = self.predict_class(self.transform(Image.fromarray(sperm_cell_image)).unsqueeze(0))

        kalman = self.sperm_cells[self.sperm_count]['kalman']
        kalman.predict()
        kalman = self.sperm_cells[self.sperm_count]['path'].append((np.array(sperm_cell['center'], dtype=np.int32), self.frame_number, predicted_class))
        kalman = self.sperm_cells[self.sperm_count]['velocities'].append(0.0)
        self.sperm_count += 1
        return

    def overall_class_prediction(self, path, last_frame):
        path_prediction = []
        accumulated_classes = torch.zeros(4)
        counter = 0
        for point, frame_number, predicted_class in path:
            if predicted_class == 4:
                continue
            if frame_number > last_frame:
                break
            accumulated_classes[predicted_class] += 1
            counter += 1
            if counter == 30:
                predicted_class = torch.argmax(accumulated_classes).item()
                path_prediction.append((point, frame_number, predicted_class))
                accumulated_classes = torch.zeros(4)
                counter = 0

        predicted_class = torch.argmax(accumulated_classes).item()
        path_prediction.append((point, frame_number, predicted_class))
        return path_prediction
    
    def calculate_velocities(self, VCL_path, VAP_VSL_path):
        average_path = calculate_average_path(VAP_VSL_path)
        velocities = []
        for index, point in enumerate(average_path[1:], start=1):
            time_interval = 1 / self.frame_rate  
            VAP_X = (average_path[index - 1][0] - point[0]) / time_interval
            VAP_Y = (average_path[index - 1][1] - point[1]) / time_interval
            velocities.append(np.sqrt(np.array(VAP_X)**2 + np.array(VAP_Y)**2))
        
        VAP = (sum(velocities) / len(VAP_VSL_path)) / self.resize_factor
        
        path_len = min(200, len(VAP_VSL_path) - 1)
        total_time = path_len / self.frame_rate
        VSL_X = (VAP_VSL_path[0][0] - VAP_VSL_path[path_len][0]) / total_time
        VSL_Y = (VAP_VSL_path[0][1] - VAP_VSL_path[path_len][1]) / total_time
        VSL = (np.sqrt(np.array(VSL_X)**2 + np.array(VSL_Y)**2)) / self.resize_factor

        velocities = []
        for index, point in enumerate(VCL_path[1:], start=1):
            time_interval = 1 / self.frame_rate  
            VCL_X = (VCL_path[index - 1][0] - point[0]) / time_interval
            VCL_Y = (VCL_path[index - 1][1] - point[1]) / time_interval
            velocities.append(np.sqrt(np.array(VCL_X)**2 + np.array(VCL_Y)**2))
 
        VCL = (sum(velocities) / len(velocities)) / self.resize_factor

        return VAP / self.video_scale, VSL / self.video_scale, VCL / self.video_scale

    def pathLength(self, path):
        path_length = 0.0
        for i in range(1, len(path)):
            distance = np.linalg.norm(np.array(path[i]) - np.array(path[i - 1]))
            path_length += distance
        return path_length / self.video_scale

    def tableOutPut (self, file):
        sperm_cells_results = []
        bundle_results = []
        more_sperm_cell = []
        rest = []

        for sperm_number, sperm_cell_info in self.sperm_cells.items():
            path = sperm_cell_info['path']
            path_points = np.array([point for point, frame_number, _ in path if frame_number < sperm_cell_info['last_frame']], dtype=np.int32)
            if len(path_points) < 50:
                continue
            path_class_prediction = self.overall_class_prediction(path, sperm_cell_info['last_frame'])

            if sum(predicted_class == 2 for _, _, predicted_class  in path_class_prediction) > ((len(path_class_prediction) - sum(predicted_class == 1 for _, _, predicted_class  in path_class_prediction)) * 0.6):
                continue

            kalman = sperm_cell_info['kalman']
            VCL_path = np.array(kalman.log_x_head)
            VAP_VSL_path = np.array(kalman.log_x_center)
            VAP, VSL, VCL =  self.calculate_velocities(VCL_path, VAP_VSL_path)
            path_length = self.pathLength(path_points)
            if path_length < 30:
                continue

            total_classifications = len(path_class_prediction)

            if sum(predicted_class == 3 for _, _, predicted_class  in path_class_prediction) > (total_classifications * 0.7):
                sperm_cells_results.append((sperm_number, VAP, VSL, VCL, path_length))
            elif sum(predicted_class == 0 for _, _, predicted_class  in path_class_prediction) > (total_classifications * 0.6):
                bundle_results.append((sperm_number, VAP, VSL, VCL, path_length))
            elif sum(predicted_class == 1 for _, _, predicted_class  in path_class_prediction) > (total_classifications * 0.7):
                more_sperm_cell.append((sperm_number, VAP, VSL, VCL, path_length))
            else:
                rest.append((sperm_number, VAP, VSL, VCL, path_length))

        file.write("type,id,VAP,VCL,VSL,traveled_distance\n")

        for id, VAP, VSL, VCL, path_length in sperm_cells_results:
            file.write(f"single,{id},{VAP:.3f},{VCL:.3f},{VSL:.3f},{path_length:.1f}\n")

        for id, VAP, VSL, VCL, path_length in bundle_results:
            file.write(f"bundle,{id},{VAP:.3f},{VCL:.3f},{VSL:.3f},{path_length:.1f}\n")

        for id, VAP, VSL, VCL, path_length in more_sperm_cell:
            file.write(f"more sperm cell,{id},{VAP:.3f},{VCL:.3f},{VSL:.3f},{path_length:.1f}\n")
        
        for id, VAP, VSL, VCL, path_length in rest:
            file.write(f"unidentified,{id},{VAP:.3f},{VCL:.3f},{VSL:.3f},{path_length:.1f}\n")

    def plot_all_paths(self, image):
        image_VCL_paths = image.copy()
        for sperm_number, sperm_cell_info in self.sperm_cells.items():
            path = sperm_cell_info['path']
            kalman = sperm_cell_info['kalman']
            VCL_path = np.array(kalman.log_x_head)
            path_points = np.array([point for point, frame_number, _ in path if frame_number < sperm_cell_info['last_frame']], dtype=np.int32)
            path_class_prediction = self.overall_class_prediction(path, sperm_cell_info['last_frame'])
            if  len(path_points) > 50:
                if sum(predicted_class == 2 for _, _, predicted_class  in path_class_prediction) > (len(path_class_prediction) - len(path_class_prediction)/4): 
                    continue
                sperm_color = ((sperm_number * 30)%255, (sperm_number * 100)%255,(sperm_number * 50)%255)
          
                average_path = calculate_average_path(path_points)
                average_path = np.array(average_path, dtype=np.int32)

                cv2.polylines(image, [path_points], isClosed=False, color=sperm_color, thickness=2)

                cv2.line(image,path_points[0], path_points[3],(0,255,0), thickness=2)
                cv2.putText(image, str(sperm_number), path_points[0], cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), thickness=2)

                cv2.polylines(image_VCL_paths, [VCL_path], isClosed=False, color=sperm_color, thickness=2)

                cv2.line(image_VCL_paths,VCL_path[0], VCL_path[3],(0,255,0), thickness=2)
                cv2.putText(image_VCL_paths, str(sperm_number), VCL_path[0], cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), thickness=2)

        cv2.imwrite(f'{self.output_dir}sperm_paths.jpg', image)
        cv2.imwrite(f'{self.output_dir}sperm_paths_vcl.jpg', image_VCL_paths)
        return
    
    def save_results(self):
        with open(f'{self.output_dir}results.csv', 'w') as file:
            self.tableOutPut(file)

import dash
from dash import dcc, html, Input, Output, callback
import base64
import os
import zipfile
from dash.long_callback import DiskcacheLongCallbackManager
import plotly.graph_objects as go
from dash import dcc
from dash.dependencies import State
import shutil
import logging
import diskcache

logger = logging.getLogger(__name__)
logging.basicConfig(filename='logs.log', encoding='utf-8', level=logging.DEBUG)
cache = diskcache.Cache("./cache")
long_callback_manager = DiskcacheLongCallbackManager(cache)

cache.set('filename_flag', "Select Video (Max size 100MB)")
cache.set('processing_flag', False)
cache.set('reset_flag', False)

def make_progress_graph(progress, total):
    progress_graph = (
        go.Figure(data=[go.Bar(x=[progress])])
        .update_xaxes(range=[0, total],)
        .update_yaxes(
            showticklabels=False,
        )
        .update_layout(height=100, margin=dict(t=20, b=40))
    )
    return progress_graph

def make_progress_graph_videos(progress, total, step_size=1):
    progress_graph = (
        go.Figure(data=[go.Bar(x=[progress], marker_color='blue')])
        .update_xaxes(
            range=[0, total], 
            dtick=1,  
        )
        .update_yaxes(
            showticklabels=False, 
        )
        .update_layout(
            height=100, 
            margin=dict(t=20, b=40),
            xaxis=dict(
                title='Progress'
            )
        )
    )
    return progress_graph

app = dash.Dash(__name__, long_callback_manager=long_callback_manager)
# app = dash.Dash(__name__, long_callback_manager=long_callback_manager, requests_pathname_prefix="/sperm_tracking/")
app.title = 'Sperm cell tracking'  

app.layout = html.Div([
    html.H1("Sperm cell tracking"),
    html.Link(
        rel='icon',
        href=f'/ico.ico?', 
        type='image/x-icon'
    ),
    dcc.Upload(
        id='upload-file',
        children=html.Div([
            html.A('Select Video (Max size 100MB)')
        ]),
        style={
            'width': '95%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        accept='.mp4,.avi',
        multiple=False
    ),
    html.Div([
        html.Label('Video Scale (pixel/\u03BCm):'),
        dcc.Input(id='video-scale-input', type='number', value=1, step="any"),
        html.Button('Process uploaded video/s',disabled=False,  id='submit-button', n_clicks=0, style={'margin-top': '10px', 'width': '100%', 'height': '50px', 'font-size': '16px'})
    ]),
    # html.Div(
    # [
    #     html.Div(
    #         [
    #             html.H3("Number of videos processed"),
    #             html.P(id="progress_videos"),
    #             dcc.Graph(id="progress_videos_graph", figure=make_progress_graph_videos(0, 50)),
    #             dcc.Interval(id='progress_videos_interval', interval=1000, n_intervals=0)
    #         ],
    #         style={'width': '50%'}
    #     )
    # ]
    # ),
    html.Div(
    [
        html.Div(
            [
                html.H3("Number of frames processed in current video"),
                html.P(id="paragraph_id"),
                dcc.Graph(id="progress_bar_graph", figure=make_progress_graph(0, 100)),
                dcc.Interval(id='progress_interval', interval=1000, n_intervals=0)
            ]
        )
    ]
    ),
    html.Div([
        html.Button("Download final report", id="btn_download", style={'fontSize': 18}),
        dcc.Download(id="download-all")
    ]),
    html.H3("Report"),
    html.Button(
    'Reset',
    id='reset-button',
    n_clicks=0,
    style={
        'fontSize': '18px', 
        'width': '150px',    
        'textAlign': 'center',
        'margin': '10px auto',  
        'display': 'block'   
    }
    ),
    html.Div(id='output')
    
])

def process_video(video_path, video_scale, output_dir, video_number):
    logger.info(f"processing video {video_path}")
    video = cv2.VideoCapture(str(video_path))
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    resize_factor = 1
    if width < 300:
        video = video_resize(video_path, 2)
        logger.info(f"video resized")
        resize_factor = 2
    # if width > 1400:
    #     video = video_resize(video_path, 0.5)
    #     logger.info(f"video resized")
    #     resize_factor = 0.5

    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))        
    video_output = cv2.VideoWriter(f"{output_dir}video.mp4", cv2.VideoWriter_fourcc(*"mp4v"), 100, (width, height))
    frame_rate = int(video.get(cv2.CAP_PROP_FPS))
    logger.info(f"output video file created")
    sperm_detection = Detect_Sperm(video_output, video_number, height, width, video_scale, output_dir, resize_factor, frame_rate)
    sperm_detection.bbox(video)
    video_output.release()
    logger.info(f"video {video_path} has been processed")
    return

@app.callback(
    Output('progress_bar_graph', 'figure'),
    [Input('progress_interval', 'n_intervals')]
)
def update_progress_bar(n_intervals):
    progress = cache.get('progress', default=0)
    total_frames = cache.get('total_frames', default=100)
    return make_progress_graph(progress, total_frames)

# @app.callback(
#     Output('progress_videos_graph', 'figure'),
#     [Input('progress_videos_interval', 'n_intervals')]
# )
def update_progress_bar_videos(n_intervals):
    progress = 1#cache.get('progress_videos', default=0)
    total_frames = cache.get('total_videos', default=100)
    return make_progress_graph(progress, total_frames)

def process_and_store_outputs(video_path, video_scale, video_number):
    video_name = os.path.splitext(os.path.basename(video_path))[0]
    output_dir = os.path.join('all_results', video_name)
    os.makedirs(output_dir, exist_ok=True)
    output_dir += '/'

    process_video(video_path, video_scale, output_dir, video_number)

@app.callback(
    Output('output', 'children'),
    [State('upload-file', 'contents'),
     State('upload-file', 'filename'),
     State('video-scale-input', 'value')],
    [Input('submit-button', 'n_clicks')],
    prevent_initial_call=True
)
def update_output(contents, filename, n_clicks, video_scale):
    logger.info(f"started unpacking")
    if n_clicks > 0 and contents is not None:
        _, content_string = contents.split(',')
        decoded = base64.b64decode(content_string)
        cache.set('processing_flag', True)
        cache.set('filename_flag', filename)
        cache.set('reset_flag', False)
        if filename.endswith('.zip'):
            logger.info(f"zip file {filename} processing started")
            zip_path = 'submitted_videos.zip'
            with open(zip_path, 'wb') as f:
                f.write(decoded)
            if os.path.exists('videos'):
                shutil.rmtree('videos')
            with zipfile.ZipFile(zip_path, 'r') as zip_ref:
                zip_ref.extractall('videos')
                logger.info(f"all videos extracted")
                all_videos = True 
                file_name_without_extension = os.path.splitext(os.path.basename(filename))[0]

                extracted_files_dir = os.path.join('videos', file_name_without_extension)
                
                extracted_files = os.listdir(extracted_files_dir)
                for file in extracted_files:
                    file_path = os.path.join(extracted_files_dir, file)
                    if os.path.isfile(file_path):
                        if file.startswith('.'):
                            continue 
                        extension = os.path.splitext(file)[1].lower()
                        if extension not in {'.mp4', '.avi'}:
                            
                            logger.error(f"zip folder contains file that is not a video {file}")
                            all_videos = False
                            break  
                if len(extracted_files) - 1 > 50:
                    logger.error(f"zip folder contains more then 50 videos. Contains: {len(extracted_files)}")
                    return html.Div("Too many videos the maximum allowed number of videos is 50 per file")
                if all_videos:
                    logger.info(f"starting processing all videos in the zip file")
                    cache.set('total_videos', len(extracted_files) - 1)
                    # cache.set('progress_videos', 0)
                    if os.path.exists('all_results'):
                        logger.info(f"old results have been removed")
                        shutil.rmtree('all_results')
                    id = 0
                    for video in extracted_files:
                        if cache.get('reset_flag'):
                            logger.info(f"video processing has been interrupted")
                            return html.Div("Video analysis interrupted")
                        if '__MACOSX' in video:
                            continue
                        id += 1
                        video_path = os.path.join(extracted_files_dir, video)
                        cache.set('filename_flag', f"file: {filename}, current video: {video}")
                        process_and_store_outputs(video_path, video_scale, id)
                        # cache.set('progress_videos', id)
                    logger.info(f"Zip file has been processed")
                    cache.set('processing_flag', False)
                    cache.set('filename_flag', "Select Video or Compressed folder with videos (Max size 100MB)")
                    return html.Div("Video analysis finished")
                else:
                    cache.set('processing_flag', False)
                    cache.set('filename_flag', "Select Video or Compressed folder with videos (Max size 100MB)")
                    return html.Div("The ZIP file contains non-video files. Please upload a ZIP with only .mp4 or .avi video files.")

        if filename.endswith(('.mp4', '.avi')):
            logger.info(f"single video has been uploaded {filename}")
            cache.set('total_videos', 1)
            # cache.set('progress_videos', 0)
            if filename.endswith('.mp4'):
                video_path = 'uploaded_video.mp4'
            if filename.endswith('.avi'):
                video_path = 'uploaded_video.avi'
            with open(video_path, 'wb') as f:
                f.write(decoded)
            process_video(video_path, video_scale, '', 0)
            # cache.set('progress_videos', 1)

            if os.path.exists('all_results'):
                shutil.rmtree('all_results')
            video_name = os.path.splitext(os.path.basename(filename))[0]
            output_dir = os.path.join('all_results', video_name)
            os.makedirs(output_dir, exist_ok=True)
            shutil.move('sperm_paths_vcl.jpg', output_dir)
            shutil.move('sperm_paths.jpg', output_dir)
            shutil.move('results.csv', output_dir)
            shutil.move('video.mp4', output_dir)
            cache.set('processing_flag', False)
            cache.set('filename_flag', "Select Video (Max size 100MB)")

            image_path = os.path.join(output_dir, "sperm_paths.jpg")
            with open(image_path, 'rb') as f:
                img_bytes = base64.b64encode(f.read()).decode('utf-8')
                image_display = html.Img(src=f'data:image/png;base64,{img_bytes}', style={'display': 'inline-block', 'width': '40%', 'vertical-align': 'top'})

            report = os.path.join(output_dir, "results.csv")
            with open(report, 'r') as f:
                text_report_content = f.read()
                text_report = html.Pre(text_report_content, style={'display': 'inline-block', 'whiteSpace': 'pre-wrap', 'width': '40%'})
            
            return [text_report, image_display]

# @app.callback(
#     Output("submit-button", "disabled"),
#     Input('progress_videos_interval', 'n_intervals'),
#     prevent_initial_call=True
# )
def disable_button(_):
    if cache.get('processing_flag'):
        return True
    else:
        return False

# @app.callback(
#     Output('upload-file', 'children'),
#     Input('progress_videos_interval', 'n_intervals'),
#     prevent_initial_call=True
# )
def update_upload_text(_):
    return cache.get('filename_flag')
    
@app.callback(
    Output("download-all", "data"),
    Input("btn_download", "n_clicks"),
    prevent_initial_call=True,
)
def download_all(n_clicks):
    with zipfile.ZipFile("report.zip", 'w', zipfile.ZIP_DEFLATED) as zf:
        for root, dirs, files in os.walk('all_results'):
            for file in files:
                file_path = os.path.join(root, file)
                arcname = os.path.relpath(file_path, start=os.path.dirname('all_results'))
                zf.write(file_path, arcname=arcname)

    logger.info("results have been downloaded")
    return dcc.send_file("report.zip")

@app.callback(
    Output('upload-file', 'filename'),
    Input('reset-button', 'n_clicks'),
    prevent_initial_call=True
)
def reset_page(n_clicks):
    if n_clicks > 0:
        cache.set('processing_flag', False)
        cache.set('filename_flag', "Select Video (Max size 100MB)")
        cache.set('reset_flag', True)
        return None


if __name__ == '__main__':
    app.run_server(debug=True)

    # app.run(
    #     host="0.0.0.0",
    #     port=8101,
    #     debug=False,
    #     threaded=True,
    #     proxy="http://0.0.0.0:8101::https://apps.datalab.fit.cvut.cz/sperm_tracking/")